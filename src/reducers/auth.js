// export const types = {
//   LOGIN_REQUEST: 'AUTH/LOGIN_REQUEST',
//   LOGIN_SUCCESS: 'AUTH/LOGIN_SUCCESS',
//   LOGIN_FAILURE: 'AUTH/LOGIN_FAILURE',
// }

export const initialState = {
  textFieldValue: '',
  rows : [
    createData('Your course has been added to member’s wishlist', true, false, false),
    createData('Member is requesting to play your course', true, false, false),
    createData('Member has a friend that would like to play your course', true, false, false),
    createData('Member is hosting a round. Want to join?', true, false, false),
    createData('A friend you invited to join Captain’s Club is now a member', true, false, false),
    createData('You haven’t yet completed your Captain’s Club Profile', true, false, false),
    createData('What courses have you played?', true, false, false),
    createData('What courses are on your wishlist?', true, false, false),
    createData('Invite your golfing friends to join Captain’s Club', true, false, false),
    createData('Your Captain’s Club Gear has been shipped.', true, false, false),
    createData('A member has RSVP’d for your event', true, false, false),
    createData('A member has RSVP’d for your round', true, false, false),
    createData('A member has commented on your post', true, false, false),
   
  ]
  
}

function createData(type, online, email, text, item) {
   
      return  { type, online, email, text, item};
    }
export default function (state = initialState, action) {
  
  switch (action.type) {
    
    case 'textFieldChange':

        for (const key in state.rows) {
          if (state.rows[key].type===action.itemid) {
            if(action.itemname==="online")
                state.rows[key].online=action.textFieldValue
            else if(action.itemname==="email")
                state.rows[key].email=action.textFieldValue
            else
                state.rows[key].text=action.textFieldValue
          }
        }
        // console.log(state.rows);
            return {
                ...state,
                textFieldValue: action.textFieldValue,
                
            }
    case 'changeSettingCheck':
      alert();
      return { 
        ...state, 
        // isLoading: true, 
        // error: null 
      }
    // case types.LOGIN_SUCCESS:
    //   return { ...state, isLoading: false, user: action.data }
    // case types.LOGIN_FAILURE:
    //   return { ...state, isLoading: false, error: action.error }
    default:
      return state
  }
}
